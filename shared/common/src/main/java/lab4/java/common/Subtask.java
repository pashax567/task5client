package lab4.java.common;

import java.io.Serializable;

public class Subtask implements Serializable {
    private String name = "";
    private String description = "";
    public enum Status {ACTIVE, COMPLETED, ARCHIVED};
    private Status status;

    private int id = 0;

    Subtask(){}
    public Subtask(String name, String description) {
        this.name = name;
        this.description = description;
        status = Status.ACTIVE;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String toString() {
        return id + ". " + name + (!description.isEmpty() ? String.format(" (%s) ", description): "");
    }
}
