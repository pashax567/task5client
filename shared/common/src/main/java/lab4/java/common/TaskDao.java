package lab4.java.common;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public interface TaskDao {
    // Добавление задачи
    void insert(Task task) throws IOException;

    // Изменение задачи
    void update(Task task) throws IOException;

    //Подключение
    void initStreams(Socket socket);

    // Удаление задачи
    void delete(Task task) throws IOException;

    String getStrForUpd() throws IOException;

    // Получить список всех задач
    ArrayList<Task> getAll(long limit, long offset) throws IOException, ClassNotFoundException;

    /// ФИЛЬТРАЦИЯ ///

    // Получить список всех незаверешнных задач, отсортированных по названию по возрастанию
    ArrayList<Task> getAllNotCompleted() throws IOException, ClassNotFoundException;

    // Получить список всех незавершенных задач, отмеченных выбранным тегом, отсортированных по названию по возрастанию
    ArrayList<Task> getAllNotCompletedWithTag(String tag) throws IOException, ClassNotFoundException;

    // Получить список всех незавершенных просроченных задач и задач со сроком выполнения в ближайшую неделю,
    // отсортированных по сроку выполнения по возрастанию, по названию по возрастанию
    ArrayList<Task> getAllNotCompletedOverdueAndForWeek() throws IOException, ClassNotFoundException;

    // Получить список всех завершенных задач, отсортированных по возрастснию
    ArrayList<Task> getAllCompleted() throws IOException, ClassNotFoundException;

    //Задачи на ближайший месяц, помеченные выбираемым тегом и содержащие в тексте описания указываемую подстроку.
    ArrayList<Task> getAllWithTagAndSubstrInDescription(String tag, String substr) throws IOException, ClassNotFoundException;

    //Незавершённые задачи, в которых по крайней мере половина подзадач завершена.
    ArrayList<Task> getAllNotCompletedWhereHalfNotCompleted() throws IOException, ClassNotFoundException;

    //Просроченные задачи, отмеченные по крайней мере одним из
    // трёх самых часто используемых тегов среди всех задач
    ArrayList<Task> getAllNotCompletedOverdueAndWithPopularTag() throws IOException, ClassNotFoundException;

    // Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом.
    ArrayList<Task> getThreeTasksWithNearestDeadlineWithTag(String tag) throws IOException, ClassNotFoundException;

    // Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом
    ArrayList<Task> getFourTasksWithMostDistantDeadlineWithoutTags() throws IOException, ClassNotFoundException;
}
