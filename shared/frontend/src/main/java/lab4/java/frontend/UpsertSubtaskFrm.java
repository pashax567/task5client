package lab4.java.frontend;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lab4.java.common.Subtask;
import lab4.java.common.Task;

import java.net.URL;
import java.util.ResourceBundle;

public class UpsertSubtaskFrm {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField tfdName;

    @FXML
    private TextField tfdDescription;

    @FXML
    private Button btnUpsert;

    @FXML
    void initialize(MainFrm.actFrm act, Task task, int indSubtask) {
        btnUpsert.setText(act == MainFrm.actFrm.INSERT ? "Добавить" : "Изменить");
        if (act == MainFrm.actFrm.UPDATE) {
            tfdName.setText(task.getSubtasks().get(indSubtask).getName());
            tfdDescription.setText(task.getSubtasks().get(indSubtask).getDescription());
        }
        btnDisable();
        tfdName.setOnKeyReleased(event -> btnDisable());
        btnUpsert.setOnAction(event -> {
            if (act == MainFrm.actFrm.INSERT)
                addTask(task);
            else
                updTask(task, indSubtask);
        });
    }

    private void btnDisable(){
        btnUpsert.setDisable(tfdName.getText().isEmpty());
    }

    private void updTask(Task task, int indSubtask) {
        Subtask st = getNewSubtask();
        int newID=0;
        for (Subtask subtask: task.getSubtasks()) {
            if(newID<subtask.getId())
                newID=subtask.getId();
        }
        newID++;
        st.setId(newID);
        task.getSubtasks().set(indSubtask, st);
        MainFrm.getAnything(1, 0, task);
        close();
    }

    private void addTask(Task task) {
        if (task.getSubtasks().stream().anyMatch(x -> x.getName().equalsIgnoreCase(tfdName.getText())))
            return;
        Subtask st = getNewSubtask();
        int newID=0;
        for (Subtask subtask: task.getSubtasks()) {
            if(newID<subtask.getId())
                newID=subtask.getId();
        }
        newID++;
        st.setId(newID);
        task.getSubtasks().add(st);
        close();
        MainFrm.getAnything(1, 0, task);
    }

    private Subtask getNewSubtask()  {
        return new Subtask(tfdName.getText().trim(), tfdDescription.getText());
    }


    private void close() {
        // close the form
        btnUpsert.getScene().getWindow().hide();
    }
}
