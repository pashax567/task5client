package lab4.java.frontend;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.function.UnaryOperator;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lab4.java.common.TaskDao;

public class ConnectionFrm {
    @FXML
    private TextField tfdHost;

    @FXML
    private TextField tfdPort;

    @FXML
    private Button btnConnect;

    @FXML
    private Label lblMess;

    static TaskDao td;

    @FXML
    void initialize() {
        btnConnect.setOnAction(event -> clickBtnConnect());
        tfdHost.setOnKeyReleased(event -> enableBtnConnect());
        tfdPort.setOnKeyReleased(event -> enableBtnConnect());

        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String input = change.getText();
            if (input.matches("[0-9]*")) {
                return change;
            }
            return null;
        };

        tfdPort.setTextFormatter(new TextFormatter<String>(integerFilter));
        // не работвет: tfdPort.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
    }

    private void enableBtnConnect(){
        btnConnect.setDisable(tfdPort.getText().trim().isEmpty());
    }

    private void clickBtnConnect() {
        lblMess.setText("");
        MainFrm.host = tfdHost.getText();
        MainFrm.port = tfdPort.getText();
        Socket socket;
        try {
            try {
                socket = new Socket(tfdHost.getText(), Integer.parseInt(tfdPort.getText()));
            }
            catch (SocketException e){
                lblMess.setText("Не удалось подключиться!");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        lblMess.setText("Успех!");
        lblMess.setTextFill(Paint.valueOf("#4009f7"));
        runMainFrm(socket);
    }

    private void runMainFrm(Socket socket) {
        Stage mainStage = new Stage();
        mainStage.setAlwaysOnTop(true);
        MainFrm.taskDao = td;
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mainFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStage.setTitle("Задачник");
        mainStage.setScene(new Scene(root, 930, 430));
        loader. <MainFrm>getController().initialize(socket);
        mainStage.show();
        btnConnect.getScene().getWindow().hide();
        //mainStage.setResizable(false);
    }
}
