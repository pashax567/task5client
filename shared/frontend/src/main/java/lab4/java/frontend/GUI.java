package lab4.java.frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lab4.java.common.TaskDao;

import java.io.IOException;

public class GUI extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        stage.initStyle(StageStyle.UTILITY);
        stage.setAlwaysOnTop(true);
        Parent root = FXMLLoader.load(getClass().getResource("/connectionFrm.fxml"));
        stage.setTitle(" Подключение");
        stage.setScene(new Scene(root, 258, 214));
        stage.show();
        stage.setResizable(false);
    }

    public void start(TaskDao td) {
        ConnectionFrm.td = td;
        launch();
    }
}
