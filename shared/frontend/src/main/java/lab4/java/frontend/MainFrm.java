package lab4.java.frontend;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lab4.java.common.Subtask;
import lab4.java.common.Task;
import lab4.java.common.TaskDao;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

public class MainFrm {
    @FXML
    private Label lblConnect;

    @FXML
    private Label lblTime;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab tabFilter;

    // Основная вкладка
    @FXML
    private TreeTableView<Subtask> listTasksTreeTableView;

    @FXML
    private TreeTableColumn<Subtask, String> nameColumn;

    @FXML
    private TreeTableColumn<Subtask, String> descriptionColumn;

    @FXML
    private TreeTableColumn<Subtask, String> deadlineColumn;

    @FXML
    private TreeTableColumn<Subtask, String> tagsColumn;

    @FXML
    private TreeTableColumn<Subtask, String> statusColumn;

    @FXML
    private Button btnAddTask;

    @FXML
    private Button btnUpdTask;

    @FXML
    private Button btnToFilter;

    @FXML
    private Button btnAddSubtask;

    @FXML
    private Button btnUpSubtask;

    @FXML
    private Button btnDownSubtask;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnComplete;

    @FXML
    private Spinner spinTask;

    @FXML
    private CheckBox chxShowSubtasks;

    @FXML
    private SplitPane splUpDown;

    @FXML
    private SplitPane splChArch;

    @FXML
    private Pane paneNotification;

    @FXML
    private Text lblX;

    // Вкладка фильтрации
    @FXML
    private TreeTableView<Subtask> listTasksTreeTableViewF;

    @FXML
    private TreeTableColumn<Subtask, String> nameColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> descriptionColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> deadlineColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> tagsColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> statusColumnF;

    @FXML
    private ComboBox<String> cbxFilters;

    @FXML
    private ComboBox<String> cbxTag;

    @FXML
    private TextField tfdSubstr;

    public MainFrm()  {
    }

    enum actFrm {INSERT, UPDATE}


    static TaskDao taskDao;
    private Socket socket = null;
    static String host = "";
    static String port = "";
    @FXML
    void initialize(Socket socket)  {
        this.socket = socket;
        taskDao.initStreams(socket);

        // Первая вкладка
        listTasksTreeTableView.setOnMouseReleased(event -> changeEnableButton());
        listTasksTreeTableView.setOnKeyReleased(event -> changeEnableButton());
        cbxTag.setOnAction(event -> printFilterLists());
        btnAddTask.setOnAction(event -> addTask());
        btnDelete.setOnAction(event -> archiveTaskOrSubtask());
        btnUpdTask.setOnAction(event -> updTaskOrSubtask());
        btnToFilter.setOnAction(event -> tabPane.getSelectionModel().select(tabFilter));
        btnAddSubtask.setOnAction(event -> addSubtask());
        btnComplete.setOnAction(event -> completeTaskOrSubtask());
        btnUpSubtask.setOnAction(event -> changeOrderSubtask());
        btnDownSubtask.setOnAction(event -> changeOrderSubtask());
        chxShowSubtasks.setOnAction(event -> printListOnTreeTable());
        splChArch.setStyle("-fx-box-border: transparent;");
        splUpDown.setStyle("-fx-box-border: transparent;");
        lblX.setOnMouseReleased(event -> paneNotification.setOpacity(0));

        // Вторая вкладка
        cbxTag.setVisible(false);
        tfdSubstr.setVisible(false);
        tabFilter.setOnSelectionChanged(event -> {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printFilterLists();});

        tfdSubstr.setOnAction(event -> printFilterLists());
        tfdSubstr.setOnKeyReleased(event -> printFilterLists());

        cbxFilters.getItems().setAll(
                "Список незавершенных",
                "Список незавершенных просроченных + на ближайшую неделю",
                "Список незавершенных по тегу",
                "Список завершенных",
                "Список задач на ближайший месяц + тег + подстрока",
                "Список незаврешнных    задач, в которых по крайней мере половина подзадач завершена",
                "Просроченные + отмеченные хотя бы одним из 3-х самых популярных тегов",
                "Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом",
                "Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом");
        cbxFilters.getSelectionModel().select(0);
        cbxFilters.setOnAction(event -> printFilterLists());

        SpinnerValueFactory<Integer> spinnerValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,5);
        this.spinTask.setValueFactory(spinnerValueFactory);
        spinTask.setOnKeyReleased(event -> printListOnTreeTable());
        spinTask.setOnMouseReleased(event -> printListOnTreeTable());

        printListOnTreeTable();

        new Thread(() ->
        {
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (true) {
                if (tabPane.getSelectionModel().isSelected(1))
                    continue;
                String serverInput = (String) getAnything(-1, 0,null);
                if (serverInput == null || !serverInput.equalsIgnoreCase("GO")) continue;
                System.out.println(serverInput);
                printListOnTreeTable();
                showNotification();
                try {
                    TimeUnit.MILLISECONDS.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread (() -> {
            while (true){
                try {
                    Platform.runLater(() -> lblTime.setText(String.valueOf(LocalTime.now().format(DateTimeFormatter.ofPattern("  HH:mm:ss", Locale.US)))));
                }
                catch (ConcurrentModificationException e){}
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread (() -> {
            while (true) {
                try {
                    Platform.runLater(() -> tabPane.setDisable(flagConnect));
                    Platform.runLater(() -> lblConnect.setText(flagConnect ? "Ожидание соединения..." : "Соединение с сервером установлено"));
                } catch (ConcurrentModificationException e) {}
                try {
                    TimeUnit.MILLISECONDS.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void showNotification(){
            for (int i = 0; i < 100; i++) {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                paneNotification.setOpacity(paneNotification.getOpacity()+0.01);
            }
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < 100; i++) {
                try {
                    TimeUnit.MILLISECONDS.sleep(40);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                paneNotification.setOpacity(paneNotification.getOpacity()-0.01);
            }
    }
    public static volatile boolean flagConnect = false;
    static synchronized Object getAnything(int i, int limit, Task task){
        switch (i){
            case -1:
                String res = "";
                try{
                    res = taskDao.getStrForUpd();
                } catch (IOException e){
                    flagConnect = true;
                    Socket newSocket = null;
                    try {
                        newSocket = new Socket(host, Integer.parseInt(port));
                    } catch (IOException ex){ return null;}
                    taskDao.initStreams(newSocket);
                    return null;
                }
                flagConnect = false;
                return  res;

            case 0:
                try {
                    taskDao.insert(task);
                } catch (IOException e) {
                    flagConnect = true;
                    repeat(task, actFrm.INSERT);
                }
                flagConnect = false;

                break;
            case 1:
                try {
                    taskDao.update(task);
                } catch (IOException e) {
                    flagConnect = true;
                    repeat(task, actFrm.UPDATE);
                }
                flagConnect = false;
                break;
            case 2:
                ArrayList<Task> result = new ArrayList <>();
                try {
                    result = taskDao.getAll(limit, 0);
                } catch (IOException | ClassNotFoundException e) {
                    flagConnect = true;
                    Socket newSocket = null;
                    try {
                        newSocket = new Socket(host, Integer.parseInt(port));
                    } catch (IOException ex){ return new ArrayList<>();}
                    taskDao.initStreams(newSocket);
                }
                flagConnect = false;
                return result;
        }
        return null;
    }

    static void repeat(Task task, actFrm act){

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.APPLICATION_MODAL);
        ((Stage)alert.getDialogPane().getScene().getWindow()).setAlwaysOnTop(true);
        alert.setTitle("Подтверждение действия");
        alert.setHeaderText("Соеднинение с сервером прервано...");
        alert.setContentText("Повтороить попытку?");
//
        Optional<ButtonType> option = alert.showAndWait();

        if (option.get() == null  || option.get() == ButtonType.CANCEL) {
            // закрываешь программу
        } else if (option.get() == ButtonType.OK) {
            Socket newSocket = null;
            try {
                newSocket = new Socket(host, Integer.parseInt(port));
            } catch (IOException e) {
                repeat(task, act);
                return;
            }
            taskDao.initStreams(newSocket);
            if (act == actFrm.INSERT)
                getAnything(0, 0, task);
            else getAnything(1, 0, task);
        }
    }

    // Добавление задачи
    private void addTask()  {
        createAndShowStageUpsertTask("Добавить задачу",null, actFrm.INSERT);
    }

    // Добавление подзадачи
    private void addSubtask()  {
        Task task = (Task) listTasksTreeTableView.getSelectionModel().getSelectedItem().getValue();
        createAndShowStageUpsertSubask("Добавить подзадачу", task, -1, actFrm.INSERT);
    }

    // Изменение задачи/подзадачи
    private void updTaskOrSubtask() {
        TreeItem <Subtask> selectedItem = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        TreeItem <Subtask> parentItem = selectedItem.getParent();
        if (selectedItem.getValue() instanceof Task)
            createAndShowStageUpsertTask("Изменить задачу", (Task) selectedItem.getValue(), actFrm.UPDATE);
        else
            createAndShowStageUpsertSubask("Изменить подзадачу", (Task) parentItem.getValue(),
                    parentItem.getChildren().indexOf(selectedItem), actFrm.UPDATE);
    }

    private void changeStatus(Subtask.Status status) {
        TreeItem <Subtask> selectedItem = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        TreeItem <Subtask> parentItem = selectedItem.getParent();
        if (selectedItem.getValue() instanceof Task) {
            Task task = (Task) selectedItem.getValue();
            task.setStatus(status);
            getAnything(1, 0, task);
        }
        else {
            Task task = (Task) parentItem.getValue();
            int indSubtask = parentItem.getChildren().indexOf(selectedItem);
            task.getSubtasks().get(indSubtask).setStatus(status);
            getAnything(1, 0, task);
        }
        printListOnTreeTable();
    }

    // Завершение задачи/подзадачи
    private void completeTaskOrSubtask() {
        changeStatus(Subtask.Status.COMPLETED);
    }

    // Удаление задачи/подзадачи
    private void archiveTaskOrSubtask() {
        changeStatus(Subtask.Status.ARCHIVED);
    }

    // Печать списков
    private void printFilterLists() {
        /*
              0.  "Список незавершенных",
              1.  "Список незавершенных просроченных + на ближайщую неделю",
              2.  "Список незавершенных по тегу",
              3.  "Список заверщенных",
              4.  "Список задач на ближайший месяц + тег + подстрока",
              5.  "Список незаврешнных задач, в которых по крайней мере половина подзадач завершена",
              6.  "Просроченные + отмеченные хотябы одним из 3-х самых популярных тегов",
              7.  "Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом",
              8.  "Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом"
        */

        int countTaskView = 50;
        cbxTag.getItems().setAll(((ArrayList<Task>) Objects.requireNonNull(getAnything(2, countTaskView, null))).stream()
                .flatMap(x -> x.getTags().stream().filter(y -> !y.isEmpty())).distinct().collect(Collectors.toList()));
        int indFilter = cbxFilters.getSelectionModel().getSelectedIndex();
        cbxTag.setVisible(indFilter == 2 || indFilter == 4 || indFilter == 7);
        tfdSubstr.setVisible(indFilter == 4);
        try {

        switch (indFilter) {
            case 0:
                printFilterList(taskDao.getAllNotCompleted());
                break;
            case 1:
                printFilterList(taskDao.getAllNotCompletedOverdueAndForWeek());
                break;
            case 2:
                try {
                    Optional <ArrayList> list = Optional.ofNullable(taskDao.getAllNotCompletedWithTag(cbxTag.getValue().trim()));
                    printFilterList(list.orElseGet(ArrayList::new));
                } catch (NullPointerException e) {
                    printFilterList(new ArrayList <>());
                }
                break;
            case 3:
                printFilterList(taskDao.getAllCompleted());
                break;
            case 4:
                try {
                    if (tfdSubstr.getText().trim().isEmpty() || cbxTag.getValue().isEmpty())
                        printFilterList(new ArrayList <>());
                    else
                        printFilterList(taskDao.getAllWithTagAndSubstrInDescription(cbxTag.getValue(), tfdSubstr.getText().trim()));
                } catch (NullPointerException e) {
                    printFilterList(new ArrayList <>());
                }
                break;
            case 5:
                printFilterList(taskDao.getAllNotCompletedWhereHalfNotCompleted());
                break;
            case 6:
                printFilterList(taskDao.getAllNotCompletedOverdueAndWithPopularTag());
                break;
            case 7:
                try {
                    Optional <ArrayList> list1 = Optional.ofNullable(taskDao.getThreeTasksWithNearestDeadlineWithTag(cbxTag.getValue().trim()));
                    printFilterList(list1.orElseGet(ArrayList::new));
                } catch (NullPointerException e) {
                    printFilterList(new ArrayList <>());
                }
                break;
            case 8:
                printFilterList(taskDao.getFourTasksWithMostDistantDeadlineWithoutTags());
                break;
        }
        } catch (IOException | ClassNotFoundException e) {
            tabPane.getSelectionModel().select(0);
            flagConnect = false;
        }
    }

    // Создание и запуск форыы добавления/изменения подзадачи
    private void createAndShowStageUpsertSubask(String title, Task task, int indSubtask, actFrm act) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/upsertSubtaskFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage upsertSubtaskFrm = new Stage();
        upsertSubtaskFrm.initModality(Modality.APPLICATION_MODAL);
        upsertSubtaskFrm.setAlwaysOnTop(true);
        upsertSubtaskFrm.setResizable(false);
        upsertSubtaskFrm.setTitle(title);
        upsertSubtaskFrm.setScene(new Scene(root, 260, 177));
        loader. <UpsertSubtaskFrm>getController().initialize(act, task, indSubtask);
        upsertSubtaskFrm.setOnHidden(event -> printListOnTreeTable());
        upsertSubtaskFrm.setOnCloseRequest(event -> printListOnTreeTable());
        upsertSubtaskFrm.show();
        //btnAddTask.getScene().getWindow().hide();
    }

    // Создание и запуск формы добавления/изменения задачи
    private void createAndShowStageUpsertTask(String title, Task task, actFrm act) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/upsertTaskFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage upsertFrm = new Stage();
        upsertFrm.initModality(Modality.APPLICATION_MODAL);
        upsertFrm.setAlwaysOnTop(true);
        upsertFrm.setResizable(false);
        upsertFrm.setTitle(title);
        upsertFrm.setScene(new Scene(root, 330, 240));

        loader. <UpsertTaskFrm>getController().initialize(act, task);
        upsertFrm.setOnHidden(event -> {
            try {
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printListOnTreeTable();
        });
        upsertFrm.setOnCloseRequest(event -> printListOnTreeTable());
        upsertFrm.show();
        //btnAddTask.getScene().getWindow().hide();
    }

    // Печать текущего списка
    private synchronized void printListOnTreeTable(){
        btnUpdTask.setDisable(true);
        btnDelete.setDisable(true);
        btnComplete.setDisable(true);
        btnAddSubtask.setDisable(true);
        btnUpSubtask.setVisible(false);
        btnDownSubtask.setVisible(false);
        btnAddSubtask.setVisible(true);
        printTreeTable((ArrayList<Task>) getAnything(2, (Integer)spinTask.getValue(), null), listTasksTreeTableView, nameColumn, descriptionColumn, deadlineColumn, tagsColumn, statusColumn);
    }

    // Печать фильтрованных списков
    private void printFilterList(ArrayList<Task> tasks){
        printTreeTable(tasks, listTasksTreeTableViewF, nameColumnF, descriptionColumnF, deadlineColumnF, tagsColumnF, statusColumnF);
    }

    // Вывод дерева
    private synchronized void printTreeTable(ArrayList<Task> tasks, TreeTableView <Subtask> listTasksTreeTableView,
                                TreeTableColumn <Subtask, String> nameColumn, TreeTableColumn <Subtask, String> descriptionColumn,
                                TreeTableColumn <Subtask, String> deadlineColumn, TreeTableColumn <Subtask, String> tagsColumn,
                                TreeTableColumn <Subtask, String> statusColumn) {
        TreeItem <Subtask> root = new TreeItem <>(null);
        try {
            Platform.runLater(() -> listTasksTreeTableView.setRoot(root));
        }
        catch (ConcurrentModificationException e){
            return;
        }
        listTasksTreeTableView.setShowRoot(false);

        for (Task task : tasks) {
            TreeItem <Subtask> taskTreeItem = new TreeItem <>(task);
            for (Subtask subtaskTreeItem : task.getSubtasks())
                taskTreeItem.getChildren().add(new TreeItem <>(subtaskTreeItem));
            taskTreeItem.setExpanded(chxShowSubtasks.isSelected());
            root.getChildren().add(taskTreeItem);
        }

        // Заносим значение в столбец "название"
        nameColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getParent().getValue() == null
                ? "  " + (param.getValue().getValue().getId()+1) + ". " + param.getValue().getValue().getName()          // Задача с маленьким отступом
                : "     " + (param.getValue().getValue().getId()+1) + ". " + param.getValue().getValue().getName()));    // Подзачача с большим отступом

        // Заносим значение в столбец "описание"
        descriptionColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue().getDescription()));


        // Заносим значение в столбец "дедлайн"
        deadlineColumn.setCellValueFactory(param -> of(new SimpleStringProperty(
                param.getValue().getValue() instanceof Task && ofNullable(((Task) param.getValue().getValue()).getDeadline()).isPresent()
                        ? ((Task) param.getValue().getValue()).getDeadline().format(DateTimeFormatter.ofPattern("dd.MM.yyyy (E)")) : ""))
                        .orElse(new SimpleStringProperty("")));

        // Заносим значение в столбец "теги"
        tagsColumn.setCellValueFactory(param ->
                of(new SimpleStringProperty(param.getValue().getValue() instanceof Task
                        ? ((Task) param.getValue().getValue()).getTags()
                        .stream().filter(x -> !x.trim().isEmpty())
                        .map(x -> " #" + x).reduce((x, y) -> x + y).orElse("") : "")).orElse(new SimpleStringProperty("")));

        // Делаем ячейку для
//        statusColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(statusColumn));
//        statusColumn.setCellValueFactory(param -> new SimpleBooleanProperty(param.getValue().getValue().getStatus()));

        statusColumn.setCellValueFactory(param -> new SimpleStringProperty(
                param.getValue().getValue().getStatus() == Subtask.Status.ACTIVE ? "Н" :
                        param.getValue().getValue().getStatus() == Subtask.Status.COMPLETED ? "З" : "А"));
        //listTasksTreeTableView.lookupAll()
    }

    /// Другие обработчики
    private void changeOrderSubtask(){
        TreeItem<Subtask> si = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        Optional<Subtask> st = Optional.ofNullable(si.getParent().getValue());
        if (!st.isPresent()) return;
        Task task = (Task) st.get();
        int indSubtask = task.getSubtasks().indexOf(si.getValue());
        if  (btnUpSubtask.isFocused())
            swapSubtask(task, indSubtask, indSubtask - 1);
        else
            swapSubtask(task, indSubtask, indSubtask + 1);
        getAnything(1, 0, task);
        printListOnTreeTable();
    }

    private void swapSubtask(Task task, int indSubtask, int i) {
        Subtask temp = task.getSubtasks().get(indSubtask);
        task.getSubtasks().set(indSubtask, task.getSubtasks().get(i));
        task.getSubtasks().set(i, temp);
    }

    private void changeEnableButton() {
        Optional <TreeItem <Subtask>> si = Optional.ofNullable(listTasksTreeTableView.getSelectionModel().getSelectedItem());
        if (!si.isPresent()) return;
        Subtask subtask = si.get().getValue();

        if (subtask instanceof Task) {

            btnAddSubtask.setVisible(true);
            btnUpSubtask.setVisible(false);
            btnDownSubtask.setVisible(false);
            btnAddSubtask.setDisable(false);
        } else {
            btnAddSubtask.setVisible(false);
            btnDownSubtask.setVisible(true);
            btnUpSubtask.setVisible(true);
            int ind = listTasksTreeTableView.getSelectionModel().getSelectedItem().getParent().getChildren()
                    .indexOf(listTasksTreeTableView.getSelectionModel().getSelectedItem());
            int count = listTasksTreeTableView.getSelectionModel().getSelectedItem().getParent().getChildren().size() - 1;
            if (ind == 0 && ind == count) {
                btnDownSubtask.setDisable(true);
                btnUpSubtask.setDisable(true);
            } else if (ind == 0) {
                btnDownSubtask.setDisable(false);
                btnUpSubtask.setDisable(true);
            } else if (ind == count) {
                btnDownSubtask.setDisable(true);
                btnUpSubtask.setDisable(false);
            } else {
                btnDownSubtask.setDisable(false);
                btnUpSubtask.setDisable(false);
            }
        }
        btnComplete.setDisable(subtask.getStatus() == Subtask.Status.COMPLETED  ||
                subtask.getStatus() == Subtask.Status.ARCHIVED);
        btnDelete.setDisable(subtask.getStatus() == Subtask.Status.ARCHIVED);
        btnUpdTask.setDisable(subtask.getStatus() == Subtask.Status.ARCHIVED);
        btnAddSubtask.setDisable(subtask.getStatus() == Subtask.Status.ARCHIVED);
    }
}
