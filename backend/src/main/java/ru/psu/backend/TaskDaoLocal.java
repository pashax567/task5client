package ru.psu.backend;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lab4.java.common.Task;
import lab4.java.common.TaskDao;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class TaskDaoLocal implements TaskDao {

    private DataOutputStream dos;
    private DataInputStream dis;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;

    public TaskDaoLocal(){

    }

    @Override
    // Подцепляемся к серверу
    public void initStreams(Socket socket) {
        try {
            dos = new DataOutputStream(socket.getOutputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    // -1; Беспрерывный запрос раз в 2 секунды на необходимость обновления списка задач
    public String getStrForUpd() throws IOException {
        dos.writeInt(-1);
        return dis.readUTF();
    }

    @Override
    // 0; Добавление задачи
    public void insert(Task task) throws IOException {
        dos.writeInt(0);
        oos.writeObject(task);
    }

    @Override
    // 1; Обновление задачи
    public void update(Task task) throws IOException {
        dos.writeInt(1);
        oos.writeObject(task);
    }

    @Override
    // 2; Получить все задачи
    public synchronized ArrayList<Task> getAll(long limit, long offset) throws IOException, ClassNotFoundException {
        dos.writeInt(2);
        dos.writeLong(limit);
        dos.writeLong(offset);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 3;
    public ArrayList<Task> getAllNotCompleted() throws IOException, ClassNotFoundException {
        dos.writeInt(3);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 4;
    public ArrayList<Task> getAllNotCompletedWithTag(String tag) throws IOException, ClassNotFoundException {
        dos.writeInt(4);
        dos.writeUTF(tag);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 5;
    public ArrayList<Task> getAllNotCompletedOverdueAndForWeek() throws IOException, ClassNotFoundException {
        dos.writeInt(5);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 6;
    public ArrayList<Task> getAllCompleted() throws IOException, ClassNotFoundException {
        dos.writeInt(6);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 7;
    public ArrayList<Task> getAllWithTagAndSubstrInDescription(String tag, String substr) throws IOException, ClassNotFoundException {
        dos.writeInt(7);
        dos.writeUTF(tag);
        dos.writeUTF(substr);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 8;
    public ArrayList<Task> getAllNotCompletedWhereHalfNotCompleted() throws IOException, ClassNotFoundException {
        dos.writeInt(8);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 9;
    public ArrayList<Task> getAllNotCompletedOverdueAndWithPopularTag() throws IOException, ClassNotFoundException {
        dos.writeInt(9);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 10;
    public ArrayList<Task> getThreeTasksWithNearestDeadlineWithTag(String tag) throws IOException, ClassNotFoundException {
        dos.writeInt(10);
        dos.writeUTF(tag);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 11;
    public ArrayList<Task> getFourTasksWithMostDistantDeadlineWithoutTags() throws IOException, ClassNotFoundException {
        dos.writeInt(11);
        return (ArrayList <Task>) ois.readObject();
    }

    @Override
    // 12; Удалить задачу (не нужная функция, осталась для удобства)
    public void delete(Task task) throws IOException {
            dos.writeInt(12);
            oos.writeObject(task);
    }
}
