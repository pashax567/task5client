import lab4.java.common.TaskDao;
import lab4.java.frontend.GUI;
import ru.psu.backend.TaskDaoLocal;

public class App {
    public static void main(String[] args) {
        GUI gui = new GUI();
        TaskDao taskDao = new TaskDaoLocal();
        gui.start(taskDao);
    }
}
